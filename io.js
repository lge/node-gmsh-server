var port = require('./sys-conf.js').port;
var io = require('socket.io').listen(port);
console.log('listen to port ' + port);
module.exports = io;
